﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Challenge1
{
    /// <summary>
    /// Логика взаимодействия для AddCustomerWindow.xaml
    /// </summary>
    public partial class AddCustomerWindow : Window
    {
        private AddCustomerWindow()
        {
            InitializeComponent();
        }

        public AddCustomerWindow(Customer newCustomer) : this()
        {
            addCustomerButton.Click += delegate
            {
                string firstNameString = firstNameTextBox.Text;
                string lastNameString = lastNameTextBox.Text;
                string surNameString = surNameTextBox.Text;
                string phoneNumberString = phoneNumberTextBox.Text;
                string emailString = emailTextBox.Text;
                if (!(string.IsNullOrEmpty(firstNameString) || string.IsNullOrEmpty(lastNameString)
                    || string.IsNullOrEmpty(surNameString) || string.IsNullOrEmpty(emailString)))
                {
                    bool isPhoneNumber = Regex.IsMatch(phoneNumberString, @"^(\+[0-9]{11})$");

                    if (isPhoneNumber || string.IsNullOrEmpty(phoneNumberString))
                    {
                        newCustomer.FirstName = firstNameString;
                        newCustomer.LastName = lastNameString;
                        newCustomer.SurName = surNameString;
                        newCustomer.PhoneNumber = phoneNumberString;
                        newCustomer.Email = emailString;
                        DialogResult = true;
                    }
                    else
                    {
                        MessageBox.Show("Неккоректный номер телефона");
                    }
                }
                else
                {
                    MessageBox.Show("Введите все значения");
                }
            };
        }
    }
}
