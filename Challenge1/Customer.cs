﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public class Customer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SurName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public Customer() { }

        public Customer(string firstName, string lastName, string surName,
            string phoneNumber, string email)
        {
            FirstName = firstName;
            LastName = lastName;
            SurName = surName;
            PhoneNumber = phoneNumber;
            Email = email;
        }

        public Customer(int id, string firstName, string lastName, string surName,
            string phoneNumber, string email) : this (firstName, lastName, surName, phoneNumber, email)
        {
            Id = id;
        }

    }
}
