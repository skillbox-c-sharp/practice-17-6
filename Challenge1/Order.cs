﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public class Order
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public int ProductCode { get; set; }
        public string ProductName { get; set; }

        public Order() { }

        public Order(int id, string email, int productCode, string productName)
        {
            Id = id;
            Email = email;
            ProductCode = productCode;
            ProductName = productName;
        }
    }
}
