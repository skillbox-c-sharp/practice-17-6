﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Challenge1
{
    class CustomerContext : DbContext
    { 
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Seller> Sellers { get; set; }
        public DbSet<Order> Orders { get; set; }

        public CustomerContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=Practice-17-6Db;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Customer>().HasData(
                new Customer(1, "Ivan", "Parshin", "Sergeevich", "+79069096528", "ivanparshin95@yandex.ru"),
                new Customer(2, "Semen", "Semenov", "Semenovich", "+79112544575", "semenov@gmail.com"),
                new Customer(3, "Ekaterina", "Firsanova", "Yurievna", "+79845548785", "ytr9o33@list.ru")
            );
            modelBuilder.Entity<Seller>().HasData(
                new Seller(1, "main_user", Encrypt("my_password"))
            );
            modelBuilder.Entity<Order>().HasData(
                new Order(1, "ivanparshin95@yandex.ru", 100, "Apple iPhone 13 Pro Max"),
                new Order(2, "semenov@gmail.com", 98, "Apple Watch Series 8 44mm"),
                new Order(3, "ytr9o33@list.ru", 135, "Sony PlayStation 5 Digital Edition"),
                new Order(4, "semenov@gmail.com", 184, "Timberland Glass")
            );
        }

        public string Encrypt(string plainText)
        {
            if (plainText == null) throw new ArgumentNullException("plainText");
            var data = Encoding.Unicode.GetBytes(plainText);
            byte[] encrypted = ProtectedData.Protect(data, null, DataProtectionScope.LocalMachine);
            return Convert.ToBase64String(encrypted);
        }
    }
}
