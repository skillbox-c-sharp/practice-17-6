﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Challenge1
{
    /// <summary>
    /// Логика взаимодействия для AddOnlineOrderWindow.xaml
    /// </summary>
    public partial class AddOnlineOrderWindow : Window
    {
        private AddOnlineOrderWindow()
        {
            InitializeComponent();
        }

        public AddOnlineOrderWindow(List<Customer> customers, Order newOrder) : this()
        {
            CustomerComboBox.ItemsSource = customers;
            CustomerComboBox.DisplayMemberPath = "Email";
            CustomerComboBox.SelectedValuePath = "Email";
            CustomerComboBox.SelectedIndex = 0;
            AddOrderButton.Click += delegate
            {
                string emailString = CustomerComboBox.SelectedValue.ToString();
                string productCodeString = productCodeTextBox.Text;
                string productNameString = productNameTextBox.Text;
                if(!(string.IsNullOrEmpty(productCodeString) || string.IsNullOrEmpty(productNameString)))
                {
                    if (int.TryParse(productCodeString, out int num))
                    {
                        newOrder.Email = emailString;
                        newOrder.ProductCode = num;
                        newOrder.ProductName = productNameString;
                        DialogResult = true;
                    }
                    else
                    {
                        MessageBox.Show("Неккоректный код продукта");
                    }
                }
                else
                {
                    MessageBox.Show("Введите все значения");
                }
            };
        }

    }
}
