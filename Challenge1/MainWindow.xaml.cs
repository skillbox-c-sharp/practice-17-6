﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Data.SqlClient;

namespace Challenge1
{
    public partial class MainWindow : Window
    {
        CustomerContext customerDb;
        List<OnlineOrder> onlineOrders;

        public MainWindow()
        {
            InitializeComponent();
            customerDb = new CustomerContext();
        }

        private void Prepair()
        {
            onlineOrders = new List<OnlineOrder>();
            RefreshData();
            dataGrid.ItemsSource = onlineOrders;
            StatusLabel.Content = $"Вход в систему выполнен";
            openDialogLoginButton.Visibility = Visibility.Hidden;
        }

        private void RefreshData()
        {
            onlineOrders.Clear();
            FillOnlineOrdersList();
            dataGrid.Items.Refresh();
        }

        private void FillOnlineOrdersList()
        {
            List<Customer> curCustomers = new List<Customer>();
            List<Order> curOrders = new List<Order>();

            foreach (var e in customerDb.Customers)
            {
                curCustomers.Add(e);
            }

            foreach (var e in customerDb.Orders)
            {
                Customer tempCustomer = curCustomers.First(d => d.Email == e.Email);
                onlineOrders.Add(new OnlineOrder(e.Id, tempCustomer.FirstName, tempCustomer.LastName, tempCustomer.SurName,
                    tempCustomer.PhoneNumber, e.Email, e.ProductCode, e.ProductName));
            }
        }

        private void openDialogLoginButton_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.ShowDialog();
            if (loginWindow.DialogResult.Value)
            {
                Prepair();
            }
        }

        private void MenuItemAddOrderClick(object sender, RoutedEventArgs e)
        {

            Order newOrder = new Order();
            AddOnlineOrderWindow addOrderWindow = new AddOnlineOrderWindow(customerDb.Customers.Local.ToList(), newOrder);
            addOrderWindow.ShowDialog();

            if (addOrderWindow.DialogResult.Value)
            {
                customerDb.Orders.Add(newOrder);
                customerDb.SaveChanges();
                RefreshData();
            }
        }

        private void MenuItemAddCustomerClick(object sender, RoutedEventArgs e)
        {
            Customer newCustomer = new Customer();
            AddCustomerWindow addCustomerWindow = new AddCustomerWindow(newCustomer);
            addCustomerWindow.ShowDialog();

            if (addCustomerWindow.DialogResult.Value)
            {
                customerDb.Customers.Add(newCustomer);
                customerDb.SaveChanges();
                RefreshData();
            }
        }

        private void MenuItemDeleteOrderClick(object sender, RoutedEventArgs e)
        {
            OnlineOrder orderForDelete = (OnlineOrder)dataGrid.SelectedItem;
            Order curOrder = customerDb.Orders.First(d => d.Id == orderForDelete.Id);
            customerDb.Orders.Remove(curOrder);
            customerDb.SaveChanges();
            RefreshData();
        }

        private void dataGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dataGrid.SelectedIndex == -1) return;

            dataGrid.CommitEdit();
            bool isChanged = false;
            string email = ((OnlineOrder)dataGrid.SelectedItem).Email;
            Customer customer = customerDb.Customers.First(d => d.Email == email);
            if(!customer.FirstName.Equals(((OnlineOrder)dataGrid.SelectedItem).FirstName) 
                || !customer.LastName.Equals(((OnlineOrder)dataGrid.SelectedItem).LastName)
                || !customer.SurName.Equals(((OnlineOrder)dataGrid.SelectedItem).SurName) 
                || !customer.PhoneNumber.Equals(((OnlineOrder)dataGrid.SelectedItem).PhoneNumber))
            {
                isChanged = true;
                if (!string.IsNullOrEmpty(((OnlineOrder)dataGrid.SelectedItem).FirstName))
                    customer.FirstName = ((OnlineOrder)dataGrid.SelectedItem).FirstName;
                if (!string.IsNullOrEmpty(((OnlineOrder)dataGrid.SelectedItem).LastName))
                    customer.LastName = ((OnlineOrder)dataGrid.SelectedItem).LastName;
                if (!string.IsNullOrEmpty(((OnlineOrder)dataGrid.SelectedItem).SurName))
                    customer.SurName = ((OnlineOrder)dataGrid.SelectedItem).SurName;
                bool isPhoneNumber = Regex.IsMatch(((OnlineOrder)dataGrid.SelectedItem).PhoneNumber, @"^(\+[0-9]{11})$");
                if (isPhoneNumber || string.IsNullOrEmpty(((OnlineOrder)dataGrid.SelectedItem).PhoneNumber))
                    customer.PhoneNumber = ((OnlineOrder)dataGrid.SelectedItem).PhoneNumber;
                else
                    MessageBox.Show("Неккоректный номер телефона");
            }

            customerDb.Customers.Update(customer);
            customerDb.SaveChanges();
            if(isChanged) RefreshData();
        }

        private void dataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            dataGrid.BeginEdit();
        }
    }
}
