﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static System.Formats.Asn1.AsnWriter;

namespace Challenge1
{
    public partial class LoginWindow : Window
    {
        CustomerContext customerDb;

        public LoginWindow()
        {
            InitializeComponent();
            cancelButton.Click += delegate { DialogResult = false; };
            customerDb = new CustomerContext();
        }

        private async void loginButton_Click(object sender, RoutedEventArgs e)
        {
            string login = loginTextBox.Text;
            string password = passwordTextBox.Text;
            bool isLoggined = false;

            await Task.Run(() =>
            {
                if (!(string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password)))
                {
                    Seller? user = customerDb.Sellers.FirstOrDefault(d => d.Login == login);
                    if (user == null)
                    {
                        MessageBox.Show("Неверные логин для входа");
                    }
                    else
                    {
                        if (password.Equals(Decrypt(user.Password)))
                        {
                            isLoggined = true;
                        }
                        else
                        {
                            MessageBox.Show("Неверный пароль для входа");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Введите логин и пароль для входа");
                }
            });
            if (isLoggined) DialogResult = true;
        }

        private string Decrypt(string cipher)
        {
            if (cipher == null) throw new ArgumentNullException("cipher");
            byte[] data = Convert.FromBase64String(cipher);
            byte[] decrypted = ProtectedData.Unprotect(data, null, DataProtectionScope.LocalMachine);
            return Encoding.Unicode.GetString(decrypted);
        }
    }
}
